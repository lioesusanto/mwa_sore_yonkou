package com.android.mikroskil.pkti.pabuo

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class RegisterActivity : AppCompatActivity() {

    lateinit var et_email: EditText
    lateinit var et_password: EditText
    lateinit var et_username: EditText
    lateinit var btn_register: Button

    lateinit var auth: FirebaseAuth
    lateinit var reference: DatabaseReference

    private var TAG = "RegisterActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        et_email = findViewById(R.id.email)
        et_password = findViewById(R.id.password)
        et_username = findViewById(R.id.username)
        btn_register = findViewById(R.id.register)

        auth = FirebaseAuth.getInstance()

        btn_register.setOnClickListener(
            object : View.OnClickListener {
                override fun onClick(view: View) {
                    val txt_username: String = et_username.getText().toString()
                    val txt_email: String = et_email.getText().toString()
                    val txt_password: String = et_password.getText().toString()
                    if (TextUtils.isEmpty(txt_username) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(
                            txt_password
                        )
                    ) {
                        Toast.makeText(this@RegisterActivity, "All fileds are required", Toast.LENGTH_SHORT)
                            .show()
                    } else if (txt_password.length < 6) {
                        Toast.makeText(
                            this@RegisterActivity,
                            "password must be at least 6 characters",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        register(txt_username, txt_email, txt_password)
                    }
                }
            })

    }



    private fun register(username: String, email: String, password: String) {

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val firebaseUser = auth.currentUser!!
                    val userid = firebaseUser.uid
                    reference =
                        FirebaseDatabase.getInstance().getReference("Users").child(userid)
                    val hashMap =
                        HashMap<String, String>()
                    hashMap["id"] = userid
                    hashMap["email"] = firebaseUser.email.toString()
                    hashMap["username"] = username
                    hashMap["imageURL"] = "default"
                    hashMap["status"] = "offline"
                    hashMap["search"] = username.toLowerCase()
                    reference.setValue(hashMap)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(
                                    this@RegisterActivity,
                                    "Register succesfull",
                                    Toast.LENGTH_SHORT
                                ).show()
                                finish()
//                                val intent =
//                                    Intent(this@RegisterActivity, MainActivity::class.java)
//                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
//                                startActivity(intent)
//                                finish()
                            }
                        }.addOnFailureListener{
                                Log.i(TAG, "fail upload to database")
                        }
                } else {
                    Toast.makeText(
                        this@RegisterActivity,
                        "You can't register woth this email or password",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }
}
