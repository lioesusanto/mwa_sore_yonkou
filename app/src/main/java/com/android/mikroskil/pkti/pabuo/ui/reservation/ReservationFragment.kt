package com.android.mikroskil.pkti.pabuo.ui.reservation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.android.mikroskil.pkti.pabuo.R
import kotlinx.android.synthetic.main.fragment_reservation.*
import kotlinx.android.synthetic.main.fragment_reservation.view.*


class ReservationFragment : Fragment() {

    private lateinit var shareViewModel: ReservationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        shareViewModel =
//            ViewModelProviders.of(this).get(ReservationViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_reservation, container, false)
        root.vp_reservation.adapter = ReservationViewPagerAdapter(childFragmentManager!!)
        root.vp_reservation_tab.setupWithViewPager(root.vp_reservation)
        return root


    }
}