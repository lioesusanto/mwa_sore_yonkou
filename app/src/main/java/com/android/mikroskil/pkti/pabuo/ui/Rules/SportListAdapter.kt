package com.android.mikroskil.pkti.pabuo.ui.Rules

import android.content.Intent
import android.os.Bundle
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.reservation.VenueDetail
import com.android.mikroskil.pkti.pabuo.ui.reservation.VenueDetailActivity
import com.bumptech.glide.Glide
import java.util.zip.Inflater

class SportListAdapter(val sportList: ArrayList<SportListView> ): RecyclerView.Adapter<SportListAdapter.ListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SportListAdapter.ListViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.item_sport, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return sportList.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.tvSportName.text = sportList[position].name
        Glide.with(holder.itemView.context)
            .load(sportList[position].imageUrl)
            .into(holder.ivSportPic)

        holder.itemView.setOnClickListener{
            val intent: Intent = Intent(holder.itemView.context, SportRulesActivity::class.java)
            intent.putExtra(SportRulesActivity.EXTRA_POSITION, position)
            holder.itemView.context.startActivity(intent)
        }
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)  {
        var ivSportPic = itemView.findViewById<ImageView>(R.id.iv_sportpic)
        var tvSportName = itemView.findViewById<TextView>(R.id.tv_sportname)
    }
}