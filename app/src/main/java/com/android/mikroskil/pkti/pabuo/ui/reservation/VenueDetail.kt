package com.android.mikroskil.pkti.pabuo.ui.reservation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.mikroskil.pkti.pabuo.IOnBackPressed

import com.android.mikroskil.pkti.pabuo.R

/**
 * A simple [Fragment] subclass.
 */
class VenueDetail : Fragment() {

    var venueData: Bundle? = null

    companion object{
        var EXTRA_NAME = "extra_name"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_venue_detail, container, false)

    }



}
