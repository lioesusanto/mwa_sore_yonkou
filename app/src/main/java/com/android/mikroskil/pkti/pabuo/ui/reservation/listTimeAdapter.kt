package com.android.mikroskil.pkti.pabuo.ui.reservation

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.android.mikroskil.pkti.pabuo.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class listTimeAdapter(val listTime: ArrayList<venueDetailsView>,
                      val venueName: String,
                      val venuaImage: String = ""
): RecyclerView.Adapter<listTimeAdapter.ListViewHolder>() {
    lateinit var pContext:Context
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): listTimeAdapter.ListViewHolder {
        pContext = parent.context
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_venue_listtime, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listTime.size
    }

    override fun onBindViewHolder(holder: listTimeAdapter.ListViewHolder, position: Int) {
        holder.tvTime.text =
                    listTime[position].time.substring(0,2) + "." +
                    listTime[position].time.substring(2,4) + " - " +
                    listTime[position].time.substring(4,6) + "." +
                    listTime[position].time.substring(6,8)


        if (listTime[position].available == "1"){
            holder.layoutTime.setBackgroundColor(Color.parseColor("#E0E0E0"))
        }

        holder.itemView.setOnClickListener{
            if (listTime[position].available == "0") {
                var dialog = AlertDialog.Builder(holder.itemView.context)
                    .setTitle("Reservation")
                    .setMessage("Book at " + holder.tvTime.text)
                    .setPositiveButton("Yes",
                        DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->

                            val venueReference = FirebaseDatabase
                                .getInstance()
                                .getReference("Venue")
                                .child(venueName)
                                .child("listtime")
                                .child(listTime[position].time)

                            venueReference.setValue("1")

                            val userAuth = FirebaseAuth.getInstance()

                            val userReference = FirebaseDatabase
                                .getInstance()
                                .getReference("Users")
                                .child(userAuth.uid.toString())
                                .child("bookList")

                            var hashMap = HashMap<String, String>()

                            userReference.addListenerForSingleValueEvent(object : ValueEventListener{
                                override fun onCancelled(p0: DatabaseError) {

                                }

                                override fun onDataChange(p0: DataSnapshot) {
                                    hashMap["placeName"] = venueName
                                    hashMap["time"] = holder.tvTime.text.toString()
                                    hashMap["imageUrl"] = venuaImage

                                    var venueID = listTime[position].time
                                    venueID = venueID + venueName.replace("\\s".toRegex(), "")

                                    userReference.child(venueID).setValue(hashMap)
                                        .addOnCompleteListener {
                                            Toast.makeText(
                                                holder.itemView.context,
                                                "Book Complete",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                        .addOnFailureListener {
                                            Toast.makeText(
                                                holder.itemView.context,
                                                "Book Failed",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                }

                            })

                            (pContext as Activity).finish()

                        })
                    .setNegativeButton("Cancel",
                        DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                            dialog?.dismiss()
                        })

                dialog.show()
            }
        }


    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var tvTime = itemView.findViewById<TextView>(R.id.tv_venuetime)
        var layoutTime = itemView.findViewById<LinearLayout>(R.id.layoutTime)
        var tvName = itemView.findViewById<TextView>(R.id.venue_item_title)
        init{
        }
    }
}