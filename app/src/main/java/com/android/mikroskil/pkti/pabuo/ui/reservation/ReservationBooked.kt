package com.android.mikroskil.pkti.pabuo.ui.reservation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Friends.friendsAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

/**
 * A simple [Fragment] subclass.
 */
class ReservationBooked : Fragment() {


    lateinit var reference: DatabaseReference
    private lateinit var rView: RecyclerView
    var bookedList: ArrayList<ReservationBookedView> = arrayListOf()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_reservation_booked, container, false)
        rView = root.findViewById(R.id.rv_venueBooked)

        reference = FirebaseDatabase
            .getInstance()
            .getReference("Users")
            .child(FirebaseAuth.getInstance().uid.toString())
            .child("bookList")

        reference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                bookedList.clear()
                var bookedListDetail: ReservationBookedView

                p0.children.forEach {
                    bookedListDetail = ReservationBookedView(
                        id = it.key.toString(),
                        name = it.child("placeName").value.toString(),
                        time = it.child("time").value.toString(),
                        imageURl = it.child("imageUrl").value.toString())
                    bookedList.add(bookedListDetail)
                }

                showRecycleList()
            }

        })


        return root
    }

    private fun showRecycleList() {
        rView.layoutManager = LinearLayoutManager(this@ReservationBooked.context)
        val listBookedAdapter = BookedAdapter(bookedList)
        rView.adapter = listBookedAdapter
    }


}
