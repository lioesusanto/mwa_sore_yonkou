package com.android.mikroskil.pkti.pabuo.ui.Friends


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.android.mikroskil.pkti.pabuo.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.database.ThrowOnExtraProperties

/**
 * A simple [Fragment] subclass.
 */
class friendProfile : Fragment() {

    lateinit var profileData:Bundle

    companion object{
        var EXTRA_ID = "extra_id"
        var EXTRA_USERNAME = "extra_name"
        var EXTRA_EMAIL = "extra_email"
        var EXTRA_IMAGE = "extra_image"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var root =  inflater.inflate(R.layout.fragment_friend_profile, container, false)

        var tvName = root.findViewById<TextView>(R.id.tv_name)
        var tvStatus = root.findViewById<TextView>(R.id.tv_status)
        var tvEmail = root.findViewById<TextView>(R.id.tv_email)
        var tvPhone = root.findViewById<TextView>(R.id.tv_phone)
        var ivProfile = root.findViewById<ImageView>(R.id.iv_profilepic)

        tvName.text = profileData.getString(EXTRA_USERNAME)
        tvEmail.text = profileData.getString(EXTRA_EMAIL)
        tvStatus.text = "Offline"
        tvPhone.text = "-"


        Glide.with(this)
            .load(profileData.getString(EXTRA_IMAGE))
            .apply(RequestOptions.circleCropTransform())
            .into(ivProfile)

        return root
    }


}
