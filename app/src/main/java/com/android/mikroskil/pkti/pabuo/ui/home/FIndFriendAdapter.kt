package com.android.mikroskil.pkti.pabuo.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Chat.ChatRoom
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


class FIndFriendAdapter(val listUser: ArrayList<FindFriendView>): RecyclerView.Adapter<FIndFriendAdapter.ListViewHolder>() {

    lateinit var pContext:Context
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FIndFriendAdapter.ListViewHolder {
        pContext = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_userdetail, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listUser.size
    }

    override fun onBindViewHolder(holder: FIndFriendAdapter.ListViewHolder, position: Int) {
        holder.tvName.text = listUser[position].name
        holder.tvLabel.text = listUser[position].label
        holder.tvStatus.text = listUser[position].status

        Glide.with(holder.itemView.context)
            .load(listUser[position].profilelink)
            .apply(RequestOptions.circleCropTransform())
            .into(holder.ivProfile)

        holder.btChat.setOnClickListener {

            val chatFrag = ChatRoom()
            holder.itemView.context
            val fManager = (pContext as AppCompatActivity).supportFragmentManager
            fManager.popBackStack()
            fManager.beginTransaction()
                .add(R.id.nav_host_fragment, chatFrag, ChatRoom::class.java.simpleName)
                .addToBackStack(null)
                .commit()
        }
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvName: TextView = itemView.findViewById(R.id.name)
        val tvLabel: TextView = itemView.findViewById(R.id.label)
        val tvStatus: TextView = itemView.findViewById(R.id.status)
        val ivProfile: ImageView = itemView.findViewById(R.id.profile)
        val btChat: Button = itemView.findViewById(R.id.chatbutton)

        init {

        }
    }
}