package com.android.mikroskil.pkti.pabuo.ui.home


import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Chat.ChatAdapter
import com.android.mikroskil.pkti.pabuo.ui.Rules.SportRulesActivity
import kotlinx.android.synthetic.main.item_userdetail.*
import androidx.recyclerview.widget.SnapHelper as AndroidxRecyclerviewWidgetSnapHelper

/**
 * A simple [Fragment] subclass.
 */
class fragment_findFriend : Fragment(){

    var filterData: Bundle? = null
    private lateinit var rView: RecyclerView

    var userList: ArrayList<FindFriendView> = arrayListOf()

    companion object{
        var EXTRA_GENDER = "extra_filterdata"
        var EXTRA_SPORT = "extra_sport"
    }

    lateinit var tv: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_find_friend, container, false)

        var addFilterBtn = root.findViewById<Button>(R.id.addfilter)
        var image = root.findViewById<ImageView>(R.id.map_view)
        rView = root.findViewById(R.id.rv_userderail)
        rView.setHasFixedSize(true)

        var a:SnapHelper = LinearSnapHelper()
        a.attachToRecyclerView(rView)


        userList.add(FindFriendView(name = "Eric Tandika", label = "Badminton Expert", status = "Hi Lets Play Together", profilelink = "https://firebasestorage.googleapis.com/v0/b/pabuo-504dd.appspot.com/o/erictandika.PNG?alt=media&token=90ed0566-31d7-48e2-b1ba-d74a4edb9cd4"))
        userList.add(FindFriendView(name = "Jesslyn Tan", label = "Amateur Swimmer", status = "Make each day your masterpiece.", profilelink = "https://firebasestorage.googleapis.com/v0/b/pabuo-504dd.appspot.com/o/jesslyntan.JPG?alt=media&token=fcbde871-12c4-4c52-a9fa-39829371c157"))
        userList.add(FindFriendView(name = "Selung Lin", label = "Pro Basketballer", status = "Work Hard Play Hard", profilelink = "https://firebasestorage.googleapis.com/v0/b/pabuo-504dd.appspot.com/o/selunglin.JPG?alt=media&token=34339415-3a6e-4768-aeda-c937e97674ef"))
        userList.add(FindFriendView(name = "Michelle Lim", label = "Chess Grandmaster", status = "See yout step", profilelink = "https://firebasestorage.googleapis.com/v0/b/pabuo-504dd.appspot.com/o/michellelim.JPG?alt=media&token=29560c5b-bff7-4b12-9f27-9a5ba9b57295"))

        showRecylerList()

        addFilterBtn.setOnClickListener{
            val addFilterLayout = layoutInflater.inflate(R.layout.addfilter_dialog, null)
            val addFilterDialog = AlertDialog.Builder(this.context)

            addFilterDialog.setView(addFilterLayout)
            addFilterDialog.create()
            val show: AlertDialog = addFilterDialog.show()
        }

        image.setOnClickListener {
            val intent: Intent = Intent(this@fragment_findFriend.context, MapsActivity::class.java)
            this@fragment_findFriend.context?.startActivity(intent)
        }




        return root
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        tv = view?.findViewById<TextView>(R.id.tesdata)
//    }
//
//    override fun onActivityCreated(savedInstanceState: Bundle?) {
//        super.onActivityCreated(savedInstanceState)
//        tv?.setText(filterData?.getString(EXTRA_GENDER))
//    }



    private fun showRecylerList(){
        rView.layoutManager = LinearLayoutManager(this@fragment_findFriend.context, LinearLayoutManager.HORIZONTAL, false)
        val ListUserAdapter = FIndFriendAdapter(userList)
        rView.adapter = ListUserAdapter
    }


}
