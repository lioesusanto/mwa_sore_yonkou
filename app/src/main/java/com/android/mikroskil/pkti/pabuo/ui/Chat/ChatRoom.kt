package com.android.mikroskil.pkti.pabuo.ui.Chat


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.android.mikroskil.pkti.pabuo.R

/**
 * A simple [Fragment] subclass.
 */
class ChatRoom : Fragment() {

    var chatData: Bundle? = null
    lateinit var chatText: TextView

    companion object{
        var EXTRA_ID = "extra_id"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_room, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        chatText = view?.findViewById(R.id.textView)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
//        chatText.setText(chatData?.getString(EXTRA_ID))
    }


}
