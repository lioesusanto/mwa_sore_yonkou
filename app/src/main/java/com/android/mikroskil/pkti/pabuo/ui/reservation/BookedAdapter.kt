package com.android.mikroskil.pkti.pabuo.ui.reservation

import android.content.Context
import android.text.Layout
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Chat.ChatRoom
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class BookedAdapter(var bookedList: ArrayList<ReservationBookedView>
): RecyclerView.Adapter<BookedAdapter.ListViewHolder>(), View.OnCreateContextMenuListener,
    MenuItem.OnMenuItemClickListener {
    var clickPosition: Int = 0
    lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookedAdapter.ListViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_reservationbooked, parent, false)

        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bookedList.size
    }

    override fun onBindViewHolder(holder: BookedAdapter.ListViewHolder, position: Int) {
        holder.tvTitle.text = bookedList[position].name
        holder.tvTime.text = bookedList[position].time

        Glide.with(holder.itemView.context)
            .load(bookedList[position].imageURl)
            .into(holder.ivVenue)

        clickPosition = position

        holder.itemView.setOnCreateContextMenuListener(this)


    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle = itemView.findViewById<TextView>(R.id.venue_item_title)
        var tvTime = itemView.findViewById<TextView>(R.id.venue_item_time)
        var ivVenue = itemView.findViewById<ImageView>(R.id.venue_item_image)
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        var delete: MenuItem = menu?.add(Menu.NONE, 1, 1, "Delete") ?: menu?.add("")!!
        delete.setOnMenuItemClickListener(this)
    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId){
            1 -> {
                val firebaseUser = FirebaseAuth.getInstance().currentUser
                val userid = firebaseUser?.uid
                var reference =
                    FirebaseDatabase.getInstance()
                        .getReference("Users")
                        .child(userid.toString())
                        .child("bookList")
                        .child(bookedList[clickPosition].id)

                reference.removeValue()

                reference =
                    FirebaseDatabase.getInstance()
                        .getReference("Venue")
                        .child(bookedList[clickPosition].name)
                        .child("listtime")
                        .child(bookedList[clickPosition].id.substring(0,8))

                reference.setValue(0)

                Toast.makeText(context, "Cancel book complete", Toast.LENGTH_SHORT).show()
            }
        }
        return true
    }


}