package com.android.mikroskil.pkti.pabuo.ui.Rules

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.android.mikroskil.pkti.pabuo.R
import com.bumptech.glide.Glide

class SportRulesActivity : AppCompatActivity() {

    var listSportRules: ArrayList<SportListViewDetail> = arrayListOf()

    companion object{
        var EXTRA_POSITION = "extra_position"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sport_rules)

        var dataIndex = intent.getIntExtra(EXTRA_POSITION, 1)

        listSportRules.addAll(SportData.listDataDetail)

        val ivSportImage = findViewById<ImageView>(R.id.iv_sportpic)
        val tvSportName = findViewById<TextView>(R.id.tv_sportname)
        val tvSportObject = findViewById<TextView>(R.id.tv_sportobject)
        val tvSportScoring = findViewById<TextView>(R.id.tv_sportscoring)

        Glide.with(this@SportRulesActivity)
            .load(listSportRules[dataIndex].imageUrl)
            .into(ivSportImage)

        tvSportName.text = listSportRules[dataIndex].name
        tvSportObject.text = listSportRules[dataIndex].sportObject
        tvSportScoring.text = listSportRules[dataIndex].sportScoring

    }
}
