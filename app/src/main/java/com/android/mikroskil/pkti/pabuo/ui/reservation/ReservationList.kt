package com.android.mikroskil.pkti.pabuo.ui.reservation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Friends.friendDetailsrView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

/**
 * A simple [Fragment] subclass.
 */
class ReservationList : Fragment() {

    private lateinit var rView: RecyclerView
    lateinit var auth: FirebaseAuth
    lateinit var venueReference: DatabaseReference
    var venueList: ArrayList<ReservationListView> = arrayListOf()
    lateinit var mFragmentManager: FragmentManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_reservation_list, container, false)

        rView = root.findViewById<RecyclerView>(R.id.rv_vanueList)
        rView.setHasFixedSize(true)
        auth = FirebaseAuth.getInstance()
        mFragmentManager = fragmentManager as FragmentManager
        venueReference = FirebaseDatabase.getInstance().getReference("Venue")

        venueReference.addValueEventListener(object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                venueList.clear()
                lateinit var venueDetail:ReservationListView
                p0.children.forEach{
                    venueDetail = ReservationListView(
                        name = it.child("name").value.toString(),
                        openTime = it.child("time").value.toString(),
                        imageUrl = it.child("imageUrl").value.toString()
                    )
                    venueList.add(venueDetail)
                }

                showRecyclerList()

            }

        })

        return root
    }

    private fun showRecyclerList() {
        rView.layoutManager = LinearLayoutManager(this@ReservationList.context)
        val venueListAdapter = ReservationListAdapter(venueList)
        rView.adapter = venueListAdapter
    }


}
