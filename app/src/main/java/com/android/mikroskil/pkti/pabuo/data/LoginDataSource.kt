package com.android.mikroskil.pkti.pabuo.data

import android.widget.Toast
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.data.model.LoggedInUser
import com.android.mikroskil.pkti.pabuo.ui.login.LoginActivity
import com.google.android.gms.gcm.Task
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    fun login(username: String, password: String): Result<LoggedInUser> {
        var success:Int = 0
        try {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(username, password)
                .addOnCompleteListener(OnCompleteListener {
                    if (it.isSuccessful()){
                        success = 1
//                        Toast.makeText( LoginActivity, "Username / Password is wrong", Toast.LENGTH_SHORT).show();
//                        return Result.Error(IOException("Username / Password is wrong"))
//                    }else{
//
//
                    }


                })

            if (success === 1){
                val User = LoggedInUser(java.util.UUID.randomUUID().toString(), username)
                return Result.Success(User)
            }else{
                return Result.Error(IOException("Username / Password is wrong"))
            }


            // TODO: handle loggedInUser authentication


        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}

