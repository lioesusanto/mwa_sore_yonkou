package com.android.mikroskil.pkti.pabuo.ui.Rules


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Friends.friendDetailsrView

/**
 * A simple [Fragment] subclass.
 */
class SportFragment : Fragment() {
    private lateinit var rView: RecyclerView
    var friendList: ArrayList<SportListView> = arrayListOf()
    lateinit var rvList: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root =  inflater.inflate(R.layout.fragment_sport, container, false)

        friendList.addAll(SportData.listDataSport)
        rvList = root.findViewById(R.id.rv_sportlist)

        rvList.layoutManager = LinearLayoutManager(this@SportFragment.context)
        val listSportAdapter = SportListAdapter(friendList)
        rvList.adapter = listSportAdapter


        return root
    }


}
