package com.android.mikroskil.pkti.pabuo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.android.mikroskil.pkti.pabuo.R
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso;




class HomeFragment : Fragment(), View.OnClickListener {

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var mStorageRef: StorageReference
    private lateinit var  mDatabaseRef: StorageReference

    private lateinit var findButton: Button
    private lateinit var findGenderRadio: RadioGroup
    private lateinit var findSportText: EditText

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        var ImaViewHome: ImageView = root.findViewById(R.id.home_image)
        mStorageRef = FirebaseStorage.getInstance().getReference()

        Picasso.with(context)
            .load("https://firebasestorage.googleapis.com/v0/b/pabuo-504dd.appspot.com/o/adult-1867757_1920.jpg?alt=media&token=528826e8-93ba-402b-b6cc-e7ae117e0311")
            .fit()
            .centerCrop()
            .into(ImaViewHome)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findButton = view.findViewById(R.id.fHome_btn_find)
        findGenderRadio = view.findViewById(R.id.fHome_gender_RG)
        findSportText = view.findViewById(R.id.fHome_et_sport)
        findButton.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        val mFragmentManager = fragmentManager as FragmentManager
        when(v?.id){
            R.id.fHome_btn_find -> {
                val mFindFragment =
                    fragment_findFriend()
                val mBundle = Bundle()

                val radioSelectedID = findGenderRadio.checkedRadioButtonId
                val radioButton: RadioButton = view!!.findViewById(radioSelectedID)

                mBundle.putString(fragment_findFriend.EXTRA_GENDER, radioButton.text.toString())
                mBundle.putString(fragment_findFriend.EXTRA_SPORT, findSportText.text.toString())

                mFindFragment.filterData = mBundle

                mFragmentManager
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, mFindFragment, fragment_findFriend::class.java.simpleName)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }


}

