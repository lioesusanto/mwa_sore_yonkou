package com.android.mikroskil.pkti.pabuo

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}