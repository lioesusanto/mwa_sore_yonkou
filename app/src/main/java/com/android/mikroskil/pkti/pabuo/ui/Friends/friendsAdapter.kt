package com.android.mikroskil.pkti.pabuo.ui.Friends

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.MainActivity
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Chat.ChatFragment
import com.android.mikroskil.pkti.pabuo.ui.Chat.ChatRoom
import com.android.mikroskil.pkti.pabuo.ui.home.HomeFragment
import com.android.mikroskil.pkti.pabuo.ui.reservation.VenueDetail
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class friendsAdapter(val listFriends: ArrayList<friendDetailsrView>, val fManager: FragmentManager): RecyclerView.Adapter<friendsAdapter.ListViewHolder>(),
    View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {
    private var context:Context? = null
    lateinit var profileFrag:friendProfile
    lateinit var chatFrag:ChatRoom
    lateinit var mBundle:Bundle
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): friendsAdapter.ListViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.fragment_friend_rvrow, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listFriends.size
    }

    override fun onBindViewHolder(holder: friendsAdapter.ListViewHolder, position: Int) {

        holder.tvUsername.text = listFriends[position].username
        holder.tvEmail.text = listFriends[position].email

        Glide.with(holder.itemView.context)
            .load(listFriends[position].profilelink)
            .apply(RequestOptions.circleCropTransform())
            .into(holder.ivProfile)


        chatFrag = ChatRoom()
        profileFrag = friendProfile()

        mBundle = Bundle()

        holder.itemView.setOnClickListener{
            mBundle.putString(friendProfile.EXTRA_ID, listFriends[position].id)
            mBundle.putString(friendProfile.EXTRA_USERNAME, listFriends[position].username)
            mBundle.putString(friendProfile.EXTRA_EMAIL, listFriends[position].email)
            mBundle.putString(friendProfile.EXTRA_IMAGE, listFriends[position].profilelink)

            profileFrag.profileData = mBundle

            fManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, profileFrag, friendProfile::class.java.simpleName)
                .addToBackStack(null)
                .commit()
        }

        holder.itemView.setOnLongClickListener{
            mBundle.putString(friendProfile.EXTRA_ID, listFriends[position].id)
            profileFrag.profileData = mBundle
            return@setOnLongClickListener false
        }

        holder.itemView.setOnCreateContextMenuListener(this)
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var rvFriends = itemView.findViewById<RelativeLayout>(R.id.rv_all)
        var tvUsername = itemView.findViewById<TextView>(R.id.rv_username)
        var tvEmail = itemView.findViewById<TextView>(R.id.rv_email)
        var ivProfile = itemView.findViewById<ImageView>(R.id.iv_profilepic)

        init{
        }
    }


    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        var Chat:MenuItem = menu?.add(Menu.NONE, 1, 1, "Chat") ?: menu?.add("")!!
        var unFriend:MenuItem = menu?.add(Menu.NONE, 2, 2,"Unfriend") ?: menu?.add("")!!
        Chat.setOnMenuItemClickListener(this)
        unFriend.setOnMenuItemClickListener(this)

    }

    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when(item?.itemId){
            1 -> {
                fManager
                    .beginTransaction()
                    .replace(R.id.nav_host_fragment, chatFrag, ChatRoom::class.java.simpleName)
                    .addToBackStack(null)
                    .commit()
            }
            2 -> {

                val firebaseUser = FirebaseAuth.getInstance().currentUser
                val userid = firebaseUser?.uid
                var reference =
                    FirebaseDatabase.getInstance()
                        .getReference("Users")
                        .child(userid.toString())
                        .child("Friends")
                        .child(mBundle[friendProfile.EXTRA_ID].toString())

                reference.removeValue()

                Toast.makeText(context, "Unfriend complete", Toast.LENGTH_SHORT).show()
            }
        }

        return true
    }

}