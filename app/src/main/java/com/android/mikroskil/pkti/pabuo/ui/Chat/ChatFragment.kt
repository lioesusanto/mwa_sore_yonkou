package com.android.mikroskil.pkti.pabuo.ui.Chat

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Friends.friendsAdapter
import com.android.mikroskil.pkti.pabuo.ui.Rules.SportRulesActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class ChatFragment : Fragment() {

    private lateinit var chatViewModel: ChatViewModel
    private lateinit var rView: RecyclerView
    lateinit var auth: FirebaseAuth
    lateinit var reference: DatabaseReference
    lateinit var usersReference: DatabaseReference
    lateinit var mFragmentManager: FragmentManager


    var chatList: ArrayList<chatThumbnailView> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        chatViewModel =
            ViewModelProviders.of(this).get(ChatViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_chat, container, false)

        rView = root.findViewById(R.id.chatRV)
        auth = FirebaseAuth.getInstance()
        mFragmentManager = fragmentManager as FragmentManager
        chatList.clear()
        chatList.add(chatThumbnailView(name = "Michelle Lim",lastChat ="Hi..", lastTime = "09.00"))
        chatList.add(chatThumbnailView(name = "Eric Tandika",lastChat ="Intro", lastTime = "11.00"))
        chatList.add(chatThumbnailView(name = "Jhony",lastChat ="Test 123 Test 123", lastTime = "23.00"))
        chatList.add(chatThumbnailView(name = "Riki Kurniawan",lastChat ="Hmmm", lastTime = "11.19"))
        chatList.add(chatThumbnailView(name = "Lestari",lastChat ="Mikroskil ...", lastTime = "00.01"))
        chatList.add(chatThumbnailView(name = "Riki Kurniawan",lastChat ="Hmmm", lastTime = "11.59"))
        chatList.add(chatThumbnailView(name = "Siapa ni",lastChat ="Hmmm", lastTime = "17.41"))
        chatList.add(chatThumbnailView(name = "Testing saja",lastChat ="Loh tapi", lastTime = "19.19"))


        usersReference = FirebaseDatabase.getInstance().getReference("Users").child(auth.currentUser?.uid.toString())

        rView.setHasFixedSize(true)

        usersReference.child("Chats").addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
//                chatList.clear()
//                lateinit var chatDetails:chatThumbnailView
//                p0.children.forEach{
//                    chatDetails = chatThumbnailView()
//                    chatList.add(chatDetails)
//                }

                showRecylerList()
            }


        })
        return root
    }

    private fun showRecylerList(){
        rView.layoutManager = LinearLayoutManager(this@ChatFragment.context)
        val listChatAdapter = ChatAdapter(chatList, mFragmentManager)
        rView.adapter = listChatAdapter
    }
}