package com.android.mikroskil.pkti.pabuo.ui.Rules

object SportData {

    private val SportName = arrayOf("Basketball",
        "Soccer",
        "American Football",
        "Badminton",
        "Baseball")

    private val SportImage = arrayOf("https://lh3.googleusercontent.com/fa-s-794VYH8BcFAHSibt9-SPhJindgcUr5wP7y_8W05kCOE2IuT0jNIUdOwcadvW6gJYudv2Lo=w640-h400-e365",
        "https://images.unsplash.com/photo-1555255508-f8259dbe6fa8?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
        "https://images.unsplash.com/photo-1537882111161-c3379a777c8b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
        "https://images.unsplash.com/flagged/photo-1572987337946-de0a00159d91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80",
        "https://images.unsplash.com/photo-1529768167801-9173d94c2a42?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80")

    private val SportObject = arrayOf("The object of basketball is to throw the ball (basketball) into a hoop to score points. The game is played out on a rectangular court and depending on which section of court you successfully throw a ball into the basket will depend on how many points are scored. The ball can be moved around the by dribbling or passing the ball. At the end of the game the team with the most points is declared the winner.",
        "The aim of football is to score more goals then your opponent in a 90 minute playing time frame. The match is split up into two halves of 45 minutes. After the first 45 minutes players will take a 15 minute rest period called half time. The second 45 minutes will resume and any time deemed fit to be added on by the referee (injury time) will be accordingly.",
        "The object of American football is to score more points than your opponents in the allotted time. To do this they must move the ball down the pitch in phases of play before eventually getting the ball into the ‘end zone’ for a touchdown. This can be achieved by either throwing the ball to a teammate or running with the ball.",
        "The object of badminton is to hit the shuttlecock over the net and have it land in the designated court areas. If your opponent manages to return the shuttlecock then a rally occurs. If you win this rally i.e. force your opponent to hit the shuttlecock out or into the net then you win a point. You are required to win 21 points to win a set with most matches being best of 3 sets. Points can be won on either serve.",
        "The object of baseball is to score more runs than your opponent. The idea is to hit the ball thrown at you as far as you can before running around 4 bases to complete a run. Once a player manages to get around the four bases before being tagged out, then another batter steps in.")

    private val SportScoring = arrayOf("There are three scoring numbers for basketball players. Any basket scored from outside the three point arc will result in three points being scored. Baskets scored within the three point arc will result in two points being scored. Successful free throws will result in 1 point being scored per free throw. The number of free throws will depend on where the foul was committed.",
        "To score the ball must go into your opponent’s goal. The whole ball needs to be over the line for it to be a legitimate goal. A goal can be scored with any part of the body apart from the hand or arm up to the shoulder. The goal itself consists of a frame measuring 8 feet high and 8 yards wide.",
        "When a player scores a touchdown six points are awarded to their team. A touchdown can be scored by either carrying the ball into the end zone or receiving the ball from a pass whilst in the end zone. After a touchdown has been scored the attacking team have opportunity to kick the ball for an extra point. The ball must pass between the upright posts for a successful kick.",
        "A point is scored when you successfully hit the shuttlecock over the net and land it in your opponent’s court before they hit it. A point can also be gained when your opponent hits the shuttlecock into either the net or outside the parameters.",
        "To score, a batter must hit the ball with the bat into the designated fielding area and make it around all four bases (before the fielding team is able to collect the ball and throw it to the base the batter is running to). A player can score a mandatory point if they hit a home run, which usually means the ball has left the playing area, often landing in the crowd. A player can stop at any base if they feel they might not make it to the next base before being tagged out.")

    val listDataSport: ArrayList<SportListView>
        get() {
            val list = arrayListOf<SportListView>()
            for (position in SportName.indices) {
                val sport = SportListView()
                sport.name = SportName[position]
                sport.imageUrl = SportImage[position]
                list.add(sport)
            }
            return list
        }


    val listDataDetail: ArrayList<SportListViewDetail>
        get() {
            val list = arrayListOf<SportListViewDetail>()
            for (position in SportName.indices) {
                val sport = SportListViewDetail()
                sport.name = SportName[position]
                sport.imageUrl = SportImage[position]
                sport.sportObject = SportObject[position]
                sport.sportScoring = SportScoring[position]
                list.add(sport)
            }
            return list
        }
}