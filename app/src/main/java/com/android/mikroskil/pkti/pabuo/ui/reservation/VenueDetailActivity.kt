package com.android.mikroskil.pkti.pabuo.ui.reservation

import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.Sampler
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import com.android.mikroskil.pkti.pabuo.ui.Friends.friendsAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.item_reservationlist.*
import kotlin.concurrent.timer

class VenueDetailActivity : AppCompatActivity() {
    var venueData: Bundle? = null
    var venueListTimeData: ArrayList<venueDetailsView> = arrayListOf()
    var venueImageUrl: String = ""
    var venueName: String = ""
    private lateinit var rView: RecyclerView

    companion object{
        var EXTRA_NAME = "extra_name"
    }

    lateinit var auth: FirebaseAuth
    lateinit var venueReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue_detail)

        venueData = intent.extras
        val title = findViewById<TextView>(R.id.venue_item_title)
        val image = findViewById<ImageView>(R.id.iv_venueImg)
        venueName = intent.getStringExtra(EXTRA_NAME)
        rView = findViewById(R.id.rv_venueListTime)
        title.text = venueName

        auth = FirebaseAuth.getInstance()
        venueReference = FirebaseDatabase.getInstance().getReference("Venue").child(venueName)
        venueReference.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {

                venueImageUrl = p0.child("imageUrl").value.toString()
                Glide.with(this@VenueDetailActivity)
                    .load(venueImageUrl)
                    .into(image)

                p0.child("listtime").children.forEach {
                    venueListTimeData.add( venueDetailsView(time = it.key.toString(), available = it.value.toString() ))
                }

                showRecyclerList()
            }

        })




    }

    private fun showRecyclerList() {
        rView.layoutManager = LinearLayoutManager(this)
        val listTimeAdapter = listTimeAdapter(venueListTimeData,
            intent.getStringExtra(EXTRA_NAME),
            venueImageUrl)
        rView.adapter = listTimeAdapter
    }
}