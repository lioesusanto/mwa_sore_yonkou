package com.android.mikroskil.pkti.pabuo.ui.reservation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.MainActivity
import com.android.mikroskil.pkti.pabuo.R
import com.bumptech.glide.Glide


class ReservationListAdapter(val listVenue: ArrayList<ReservationListView>): RecyclerView.Adapter<ReservationListAdapter.ListViewHolder>(){

    lateinit var mBundle: Bundle
    private  var context:Context? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReservationListAdapter.ListViewHolder {
        context = parent.context
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_reservationlist, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listVenue.size
    }

    override fun onBindViewHolder(holder: ReservationListAdapter.ListViewHolder, position: Int) {

        holder.tv_venueTitle.text = listVenue[position].name
        holder.tv_venueTime.text = listVenue[position].openTime

        Glide.with(holder.itemView.context)
            .load(listVenue[position].imageUrl)
            .into(holder.iv_venueImg)

        holder.itemView.setOnClickListener {
            mBundle = Bundle()
            mBundle.putString(VenueDetail.EXTRA_NAME, listVenue[position].name)

            val intent: Intent = Intent(holder.itemView.context, VenueDetailActivity::class.java)
            intent.putExtra(VenueDetailActivity.EXTRA_NAME, holder.tv_venueTitle.text.toString())

            holder.itemView.context.startActivity(intent)
        }

    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val iv_venueImg = itemView.findViewById<ImageView>(R.id.venue_item_image)
        val tv_venueTitle = itemView.findViewById<TextView>(R.id.venue_item_title)
        val tv_venueTime = itemView.findViewById<TextView>(R.id.venue_item_time)
        val cl_venueAll = itemView.findViewById<LinearLayout>(R.id.venue_item_all)

        init {

        }

    }



}