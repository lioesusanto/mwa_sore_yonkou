package com.android.mikroskil.pkti.pabuo.ui.Rules

data class SportListView(
    var name:String = "",
    var imageUrl:String = ""
)

data class SportListViewDetail(
    var name:String = "",
    var imageUrl:String = "",
    var sportObject:String = "",
    var sportScoring:String = ""
)