package com.android.mikroskil.pkti.pabuo.ui.Chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.android.mikroskil.pkti.pabuo.R
import kotlinx.android.synthetic.main.fragment_chat_rvrow.view.*
import org.w3c.dom.Text

class ChatAdapter( val listChat: ArrayList<chatThumbnailView>, val fManager: FragmentManager): RecyclerView.Adapter<ChatAdapter.ListViewHolder>(){
    lateinit var chatFrag:ChatRoom
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ChatAdapter.ListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.fragment_chat_rvrow, parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listChat.size
    }

    override fun onBindViewHolder(holder: ChatAdapter.ListViewHolder, position: Int) {
        holder.tvUsername.text = listChat[position].name
        holder.tvLastchat.text = listChat[position].lastChat
        holder.tvLasttime.text = listChat[position].lastTime
        chatFrag = ChatRoom()
        holder.itemView.setOnClickListener {
            fManager
                .beginTransaction()
                .replace(R.id.nav_host_fragment, chatFrag, ChatRoom::class.java.simpleName)
                .addToBackStack(null)
                .commit()
        }
    }

    class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val tvUsername: TextView = itemView.findViewById(R.id.rv_username)
        val tvLastchat: TextView = itemView.findViewById(R.id.rv_lastchat)
        val tvLasttime: TextView = itemView.findViewById(R.id.rv_lasttime)

        init {

        }
    }
}